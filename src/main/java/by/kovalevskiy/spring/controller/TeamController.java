package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Team;
import by.kovalevskiy.spring.repository.TeamDAO;
import by.kovalevskiy.spring.util.TeamVallidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("teams")
public class TeamController {
    private final TeamDAO teamDAO;
    private final TeamVallidator teamVallidator;
    @Autowired
    public TeamController(TeamDAO teamDAO, TeamVallidator teamVallidator) {
        this.teamDAO = teamDAO;
        this.teamVallidator = teamVallidator;
    }
    @GetMapping()
    public String showAllTeams (Model model){
        model.addAttribute("teams", teamDAO.showAllTeams());
        return "team/allTeams";
    }
    @GetMapping ("/team")
    public String showTeam (@RequestParam("name") String name, Model model){
        model.addAttribute("team", teamDAO.showTeam(name).get());
        return "team/team";
    }
    @GetMapping ("/{id}")
    public String showTeam (@PathVariable("id") int id, Model model){
        model.addAttribute("team", teamDAO.showTeam(id));
        return "team/team";
    }
    @GetMapping ("/new")
    public String newTeam (Model model){
        model.addAttribute("team", new Team());
        return "team/newTeam";
    }
    @PostMapping()
    public String createTeam (@ModelAttribute("team") @Valid Team team, BindingResult bindingResult){
        teamVallidator.validate(team,bindingResult);
        if (bindingResult.hasErrors()){
            return "team/newTeam";
        }
        team.setCreated(LocalDate.now());
        team.setUpdated(LocalDate.now());
        teamDAO.saveTeam(team);
        return "redirect: /teams";
    }
    @GetMapping ("/{id}/edit")
    public String editTeam (Model model, @PathVariable("id") int id){
        model.addAttribute("team", teamDAO.showTeam(id));
        return "team/editTeam";
    }
    @PostMapping ("/{id}/edit")
    public String updateTeam (@ModelAttribute("team") @Valid Team team, BindingResult bindingResult){
        teamVallidator.validate(team,bindingResult);
        if (bindingResult.hasErrors()){
            return "team/editTeam";
        }
        team.setUpdated(LocalDate.now());
        teamDAO.updateTeam(team);
        return "redirect: /teams";
    }
    @PostMapping ("/{id}/delete")
    public String deleteTeam (@PathVariable("id") int id){
        teamDAO.deleteTeam(id);
        return "redirect: /teams";
    }
}
