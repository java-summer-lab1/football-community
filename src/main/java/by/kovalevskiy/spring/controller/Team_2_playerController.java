package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Player;
import by.kovalevskiy.spring.repository.PlayerDAO;
import by.kovalevskiy.spring.repository.TeamDAO;
import by.kovalevskiy.spring.util.PlayerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("team_2_player")
public class Team_2_playerController {
    private final PlayerDAO playerDAO;
    private final TeamDAO teamDAO;
    private final PlayerValidator playerValidator;
    @Autowired
    public Team_2_playerController(PlayerDAO playerDAO, TeamDAO teamDAO, PlayerValidator playerValidator) {
        this.playerDAO = playerDAO;
        this.teamDAO = teamDAO;
        this.playerValidator = playerValidator;
    }

    @GetMapping("players_from_team/{id}")
    public String showTeamPlayers (Model model, @PathVariable("id") int id){
        model.addAttribute("numberOfTeam",id);
        model.addAttribute("team_players", playerDAO.showTeamPlayers(id));
        model.addAttribute("all_players", playerDAO.showAllPlayers());
        model.addAttribute("player", new Player());
        return "team/teamPlayers";
    }

    @PostMapping("/{player_id}/delete_from_team{team_id}")
    public String deletePlayerFromTeam (@PathVariable("player_id") int player_id,@PathVariable("team_id") int team_id){
        playerDAO.deletePlayerFromTeam(player_id,team_id);
        return "redirect: /team_2_player/players_from_team/{team_id}";
    }

    @PostMapping("/add_player_to_team_{team_id}")
    public String addPlayerToTeam (@ModelAttribute("player") Player player, @PathVariable("team_id")int team_id){
        playerDAO.addPlayerToTeam(team_id,player.getId());
        return "redirect: /team_2_player/players_from_team/{team_id}";
    }
}

