package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Comment;
import by.kovalevskiy.spring.model.Game;
import by.kovalevskiy.spring.repository.*;
import by.kovalevskiy.spring.util.GameValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("games")
public class GameController {
    private final GameDAO gameDAO;
    private final TournamentDAO tournamentDAO;
    private final PlaceDAO placeDAO;
    private final TeamDAO teamDAO;
    private final CommentDAO commentDAO;
    private final UserDAO userDAO;
    private final GameValidator gameValidator;

    @Autowired
    public GameController(GameDAO gameDAO, TournamentDAO tournamentDAO, PlaceDAO placeDAO, TeamDAO teamDAO, CommentDAO commentDAO, UserDAO userDAO, GameValidator gameValidator) {
        this.gameDAO = gameDAO;
        this.tournamentDAO = tournamentDAO;
        this.placeDAO = placeDAO;
        this.teamDAO = teamDAO;
        this.commentDAO = commentDAO;
        this.userDAO = userDAO;
        this.gameValidator = gameValidator;
    }

    @GetMapping()
    public String showAllGames(Model model) {
        model.addAttribute("games", gameDAO.showAllGames());
        return "game/allGames";
    }

    @GetMapping("/{id}")
    public String showGame(@PathVariable("id") int id, Model model) {
        Game game = gameDAO.showGame(id);
        game.statusUpdate(game);
        gameDAO.updateGameStatus(game);
        model.addAttribute("game", game);
        model.addAttribute("comments", commentDAO.showAllGameComments(id));
        model.addAttribute("new_comment", new Comment());
        model.addAttribute("author", userDAO.showAllUser());
        return "game/game";
    }

    @GetMapping("/new")
    public String newGame(Model model) {
        model.addAttribute("tournaments", tournamentDAO.showAllTournament());
        model.addAttribute("places", placeDAO.showAllPlaces());
        model.addAttribute("teams", teamDAO.showAllTeams());
        model.addAttribute("game", new Game());
        return "game/newGame";
    }

    @PostMapping()
    public String createGame(@ModelAttribute("game") @Valid Game game, BindingResult bindingResult) {
        gameValidator.validate(game,bindingResult);
        if (bindingResult.hasErrors()) {
            return "redirect: /games/new";
        }
        game.statusUpdate(game);
        game.setCreated(LocalDate.now());
        game.setUpdated(LocalDate.now());
        gameDAO.saveGame(game);
        return "redirect: /games";
    }

    @GetMapping("/{id}/edit")
    public String editGame(Model model, @PathVariable("id") int id) {
        model.addAttribute("tournaments", tournamentDAO.showAllTournament());
        model.addAttribute("places", placeDAO.showAllPlaces());
        model.addAttribute("teams", teamDAO.showAllTeams());
        model.addAttribute("game", gameDAO.showGame(id));
        return "game/editGame";
    }

    @PostMapping("/{id}/edit")
    public String updateGame(@ModelAttribute("game") @Valid Game game, BindingResult bindingResult, Model model, @PathVariable("id") int id) {
        gameValidator.validate(game,bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("tournaments", tournamentDAO.showAllTournament());
            model.addAttribute("places", placeDAO.showAllPlaces());
            model.addAttribute("teams", teamDAO.showAllTeams());
            model.addAttribute("game", gameDAO.showGame(id));
            return "game/editGame";
        }
        game.statusUpdate(game);
        game.setUpdated(LocalDate.now());
        gameDAO.updateGame(game);
        return "redirect: /games/{id}";
    }

    @PostMapping("/{id}/delete")
    public String deleteGame(@PathVariable("id") int id) {
        gameDAO.deleteGame(id);
        return "redirect: /games";
    }
}
