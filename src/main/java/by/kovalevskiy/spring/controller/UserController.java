package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.repository.UserDAO;
import by.kovalevskiy.spring.model.User_account;
import by.kovalevskiy.spring.util.User_accountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("users")
public class UserController {
    private final UserDAO userDAO;
    private final User_accountValidator userAccountValidator;

    @Autowired
    public UserController(UserDAO userDAO, User_accountValidator userAccountValidator) {
        this.userDAO = userDAO;
        this.userAccountValidator = userAccountValidator;
    }

    @GetMapping ()
    public String showAllUser (Model model){
        model.addAttribute("users", userDAO.showAllUser());
        return "user/allUsers";
    }
    @GetMapping ("/{id}")
    public String showUser (@PathVariable("id") int id, Model model){
        model.addAttribute("user", userDAO.showUser(id));
        return "user/user";
    }
    @GetMapping ("/new")
    public String newUser (Model model){
        model.addAttribute("user", new User_account());
        return "user/newUser";
    }
    @PostMapping ()
    public String createUser (@ModelAttribute("user") @Valid User_account user_account, BindingResult bindingResult){
        userAccountValidator.validate(user_account,bindingResult);
        if (bindingResult.hasErrors()){
            return "user/newUser";
        }
        user_account.setRole("user");
        user_account.setCreated(LocalDate.now());
        user_account.setUpdated(LocalDate.now());
        userDAO.saveUser(user_account);
        return "redirect: /users";
    }
    @GetMapping ("/{id}/edit")
    public String editUser (Model model, @PathVariable("id") int id){
        model.addAttribute("user", userDAO.showUser(id));
        return "user/editUser";
    }
    @PostMapping ("/{id}/edit")
    public String updateUser (@ModelAttribute("user") @Valid User_account user_account, BindingResult bindingResult){
        userAccountValidator.validate(user_account,bindingResult);
        if (bindingResult.hasErrors()){
            return "user/editUser";
        }
        user_account.setUpdated(LocalDate.now());
        userDAO.updateUser(user_account);
        return "redirect: /users";
    }
    @PostMapping ("/{id}/delete")
    public String deleteUser (@PathVariable("id") int id){
        userDAO.deleteUser(id);
        return "redirect: /users";
    }

}
