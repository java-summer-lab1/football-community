package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Place;
import by.kovalevskiy.spring.repository.PlaceDAO;
import by.kovalevskiy.spring.util.PlaceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;


@Controller
@RequestMapping("places")
public class PlaceController {
    private final PlaceDAO placeDAO;
    private final PlaceValidator placeValidator;
    @Autowired
    public PlaceController(PlaceDAO placeDAO, PlaceValidator placeValidator) {
        this.placeDAO = placeDAO;
        this.placeValidator = placeValidator;
    }
    @GetMapping()
    public String showAllPlaces (Model model){
        model.addAttribute("places", placeDAO.showAllPlaces());
        return "place/allPlaces";
    }
    @GetMapping ("/place")
    public String showPlace (@RequestParam("name") String name, Model model){
        model.addAttribute("place", placeDAO.showPlace(name).get());
        return "place/place";
    }
    @GetMapping ("/{id}")
    public String showPlace (@PathVariable("id") int id, Model model){
        model.addAttribute("place", placeDAO.showPlace(id));
        return "place/place";
    }
    @GetMapping ("/new")
    public String newPlace (Model model){
        model.addAttribute("place", new Place());
        return "place/newPlace";
    }
    @PostMapping()
    public String createPlace (@ModelAttribute("place") @Valid Place place, BindingResult bindingResult){
        placeValidator.validate(place,bindingResult);
        if (bindingResult.hasErrors()){
            return "place/newPlace";
        }
        place.setCreated(LocalDate.now());
        place.setUpdated(LocalDate.now());
        placeDAO.savePlace(place);
        return "redirect: /places";
    }
    @GetMapping ("/{id}/edit")
    public String editPlace (Model model, @PathVariable("id") int id){
        model.addAttribute("place", placeDAO.showPlace(id));
        return "place/editPlace";
    }
    @PostMapping ("/{id}/edit")
    public String updatePlace (@ModelAttribute("place") @Valid Place place, BindingResult bindingResult){
        placeValidator.validate(place,bindingResult);
        if (bindingResult.hasErrors()){
            return "place/editPlace";
        }
        place.setUpdated(LocalDate.now());
        placeDAO.updatePlace(place);
        return "redirect: /places";
    }
    @PostMapping ("/{id}/delete")
    public String deletePlace (@PathVariable("id") int id){
        placeDAO.deletePlace(id);
        return "redirect: /places";
    }
}
