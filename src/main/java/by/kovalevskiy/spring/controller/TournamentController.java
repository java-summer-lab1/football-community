package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Tournament;
import by.kovalevskiy.spring.repository.TeamDAO;
import by.kovalevskiy.spring.repository.TournamentDAO;
import by.kovalevskiy.spring.util.TournamentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;


@Controller
@RequestMapping("/tournaments")
public class TournamentController {
    private final TournamentDAO tournamentDAO;
    private final TeamDAO teamDAO;
    private final TournamentValidator tournamentValidator;
    @Autowired
    public TournamentController(TournamentDAO tournamentDAO, TeamDAO teamDAO, TournamentValidator tournamentValidator) {
        this.tournamentDAO = tournamentDAO;
        this.teamDAO = teamDAO;
        this.tournamentValidator = tournamentValidator;
    }

    @GetMapping()
    public String showAllTournament (Model model){
        model.addAttribute("tournaments", tournamentDAO.showAllTournament());
        return "tournament/allTournament";
    }
    @GetMapping ("/tournament")
    public String showTournament (@RequestParam("name") String name, Model model){
        model.addAttribute("tournament", tournamentDAO.showTournament(name).get());
        //model.addAttribute("teams",teamDAO.showAllTeams());
        return "tournament/tournament";
    }
    @GetMapping ("/{id}")
    public String showTournament (@PathVariable("id") int id, Model model){
        model.addAttribute("tournament", tournamentDAO.showTournament(id));
        //model.addAttribute("teams",teamDAO.showAllTeams(id));
        return "tournament/tournament";
    }
    @GetMapping ("/new")
    public String newTournament (Model model){
        model.addAttribute("tournament", new Tournament());
        return "tournament/newTournament";
    }
    @PostMapping()
    public String createTournament (@ModelAttribute("tournament") @Valid Tournament tournament, BindingResult bindingResult){
        tournamentValidator.validate(tournament,bindingResult);
        if (bindingResult.hasErrors()){
            return "tournament/newTournament";
        }
        tournament.setCreated(LocalDate.now());
        tournament.setUpdated(LocalDate.now());
        tournamentDAO.saveTournament(tournament);
        return "redirect: /tournaments";
    }
    @GetMapping ("/{id}/edit")
    public String editTournament (Model model, @PathVariable("id") int id){
        model.addAttribute("tournament", tournamentDAO.showTournament(id));
        return "tournament/editTournament";
    }
    @PostMapping ("/{id}/edit")
    public String updateTournament (@ModelAttribute("tournament") @Valid Tournament tournament, BindingResult bindingResult){
        tournamentValidator.validate(tournament,bindingResult);
        if (bindingResult.hasErrors()){
            return "tournament/editTournament";
        }
        tournament.setUpdated(LocalDate.now());
        tournamentDAO.updateTournament(tournament);
        return "redirect: /tournaments";
    }
    @PostMapping ("/{id}/delete")
    public String deleteTournament (@PathVariable("id") int id){
        tournamentDAO.deleteTournament(id);
        return "redirect: /tournaments";
    }
}
