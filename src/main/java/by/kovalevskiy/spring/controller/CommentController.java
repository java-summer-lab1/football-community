package by.kovalevskiy.spring.controller;

import by.kovalevskiy.spring.model.Comment;
import by.kovalevskiy.spring.repository.CommentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("comments")
public class CommentController {
    private final CommentDAO commentDAO;
    @Autowired
    public CommentController(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    @PostMapping("/game_id={id}")
    public String createComment (@PathVariable("id")int id, @ModelAttribute("comment") @Valid Comment comment,BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect: /games/{id}";
        }
        comment.setGame_id(id);
        comment.setCreated(LocalDate.now());
        comment.setUpdated(LocalDate.now());
        commentDAO.saveComment(comment);
        return "redirect: /games/{id}";
    }
    @PostMapping ("/{id}/delete")
    public String deleteComment (@PathVariable("id") int id, Model model){
        Comment comment = commentDAO.showComment(id);
        model.addAttribute("id",comment.getGame_id());
        commentDAO.deleteComment(id);
        return "redirect: /games/{id}";
    }
}
