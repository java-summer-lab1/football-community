package by.kovalevskiy.spring.controller;
import by.kovalevskiy.spring.model.Player;
import by.kovalevskiy.spring.repository.PlayerDAO;
import by.kovalevskiy.spring.util.PlayerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("players")
public class PlayerController {
    private final PlayerDAO playerDAO;
    private final PlayerValidator playerValidator;
    @Autowired
    public PlayerController(PlayerDAO playerDAO, PlayerValidator playerValidator) {
        this.playerDAO = playerDAO;
        this.playerValidator = playerValidator;
    }
    @GetMapping()
    public String showAllPlayers (Model model){
        model.addAttribute("players", playerDAO.showAllPlayers());
        return "player/allPlayers";
    }
    @GetMapping ("/{id}")
    public String showPlayer (@PathVariable("id") int id, Model model){
        model.addAttribute("player", playerDAO.showPlayer(id));
        return "player/player";
    }
    @GetMapping ("/new/{id}")
    public String newPlayer (Model model, @PathVariable("id") int id){
        Player player = new Player();
        player.setId(id);
        model.addAttribute("player", player);
        return "player/newPlayer";
    }
    @PostMapping()
    public String createPlayer (@ModelAttribute("player") @Valid Player player, BindingResult bindingResult){
        playerValidator.validate(player,bindingResult);
        if (bindingResult.hasErrors()){
            return "player/newPlayer";
        }
        player.setCreated(LocalDate.now());
        player.setUpdated(LocalDate.now());
        playerDAO.savePlayer(player);
        return "redirect: /players";
    }
    @GetMapping ("/{id}/edit")
    public String editPlayer (Model model, @PathVariable("id") int id){
        model.addAttribute("player", playerDAO.showPlayer(id));
        return "player/editPlayer";
    }
    @PostMapping ("/{id}/edit")
    public String updatePlayer (@ModelAttribute("player") @Valid Player player, BindingResult bindingResult){
        playerValidator.validate(player,bindingResult);
        if (bindingResult.hasErrors()){
            return "player/editPlayer";
        }
        player.setUpdated(LocalDate.now());
        playerDAO.updatePlayer(player);
        return "redirect: /players";
    }
    @PostMapping ("/{id}/delete")
    public String deletePlayer (@PathVariable("id") int id){
        playerDAO.deletePlayer(id);
        return "redirect: /players";
    }

}
