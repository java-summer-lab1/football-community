package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.Place;
import by.kovalevskiy.spring.repository.PlaceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
@Component
public class PlaceValidator implements Validator {
    private final PlaceDAO placeDAO;
    @Autowired
    public PlaceValidator(PlaceDAO placeDAO) {
        this.placeDAO = placeDAO;
    }
    @Override
    public boolean supports(Class<?> clazz) {
        return Place.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Place place = (Place) target;
        if(placeDAO.showPlaceByAddress(place.getAddress()).isPresent()){
            errors.rejectValue("address","","Place with that address already exists");
        }
        if(placeDAO.showPlace(place.getName()).isPresent()){
            errors.rejectValue("name","","Place with that name already exists");
        }
    }
}
