package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.User_account;
import by.kovalevskiy.spring.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class User_accountValidator implements Validator {
    private final UserDAO userDAO;
    @Autowired
    public User_accountValidator(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    @Override
    public boolean supports(Class<?> clazz) {
        return User_account.class.equals(clazz);
    }
    @Override
    public void validate(Object target, Errors errors) {
        User_account user_account = (User_account) target;
        if(userDAO.showUser(user_account.getNickname()).isPresent()){
            errors.rejectValue("nickname","", "This nickname is already taken");
        }
    }
}
