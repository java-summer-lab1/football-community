package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.Team;
import by.kovalevskiy.spring.repository.TeamDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
@Component
public class TeamVallidator implements Validator {
    private final TeamDAO teamDAO;
    @Autowired
    public TeamVallidator(TeamDAO teamDAO) {
        this.teamDAO = teamDAO;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Team.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Team team = (Team) target;
        if(teamDAO.showTeam(team.getName()).isPresent()){
            errors.rejectValue("name","","Team with that name already exists");
        }
    }
}
