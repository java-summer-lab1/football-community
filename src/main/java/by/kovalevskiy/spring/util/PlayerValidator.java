package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.Player;
import by.kovalevskiy.spring.repository.PlayerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PlayerValidator implements Validator {
    private final PlayerDAO playerDAO;
    @Autowired
    public PlayerValidator(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }
    @Override
    public boolean supports(Class<?> clazz) {
        return Player.class.equals(clazz);
    }
    @Override
    public void validate(Object target, Errors errors) {
        Player player = (Player) target;
        if(playerDAO.showPlayer(player.getName(),player.getSurname()).isPresent()){
            errors.rejectValue("surname","","A player with the same name and surname already exists");
        }
    }
}
