package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.Tournament;
import by.kovalevskiy.spring.repository.TournamentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
@Component
public class TournamentValidator implements Validator {
    private final TournamentDAO tournamentDAO;
    @Autowired
    public TournamentValidator(TournamentDAO tournamentDAO) {
        this.tournamentDAO = tournamentDAO;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Tournament.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Tournament tournament = (Tournament) target;
        if(tournamentDAO.showTournament(tournament.getName()).isPresent()){
            errors.rejectValue("name","","Tournament with that name already exists");
        }

    }
}
