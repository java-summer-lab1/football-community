package by.kovalevskiy.spring.util;

import by.kovalevskiy.spring.model.Game;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class GameValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Game.class.equals(clazz);
    }
    @Override
    public void validate(Object target, Errors errors) {
        Game game = (Game) target;
        if (game.getHome_team_name().equals(game.getGuest_team_name())){
            errors.rejectValue("home_team_name","","Team should play against another team");
        }
    }
}
