package by.kovalevskiy.spring.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Player {
    private int id;
    @NotEmpty(message = "name should not be empty")
    @Size(min = 2,max = 20,message = "allowed from 2 to 20 characters")
    private String name;
    @NotEmpty(message = "surname should not be empty")
    @Size(min = 2,max = 30,message = "allowed from 2 to 30 characters")
    private String surname;
    @Min(value = 4,message = "player should be older than 3 years")
    private int age;
    @NotEmpty(message = "city should not be empty")
    private String city;
    private LocalDate created;
    private LocalDate updated;
    public Player() {
    }
    public Player(int id, String name, String surname, int age, String city, LocalDate created, LocalDate updated) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.city = city;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }
}
