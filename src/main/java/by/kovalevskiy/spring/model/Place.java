package by.kovalevskiy.spring.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Place {
    private int id;
    @NotEmpty(message = "name should not be empty")
    @Size(min = 2,max = 100,message = "allowed from 2 to 100 characters")
    private String name;
    @NotEmpty(message = "name of city should not be empty")
    @Size(min = 2,max = 100,message = "allowed from 2 to 100 characters")
    private String city;
    @NotEmpty(message = "address should not be empty")
    @Size(min = 2,max = 100,message = "allowed from 2 to 100 characters")
    private String address;
    @Size(max = 12,message = "enter valid number")
    private String administrator_phone_number;
    private LocalDate created;
    private LocalDate updated;

    public Place() {
    }

    public Place(int id, String name, String city, String address, String administrator_phone_number, LocalDate created, LocalDate updated) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.address = address;
        this.administrator_phone_number = administrator_phone_number;
        this.created = created;
        this.updated = updated;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdministrator_phone_number() {
        return administrator_phone_number;
    }

    public void setAdministrator_phone_number(String administrator_phone_number) {
        this.administrator_phone_number = administrator_phone_number;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }
}
