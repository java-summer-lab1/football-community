package by.kovalevskiy.spring.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

public class Game {
    private int id;
    private String tournament_name;
    private String place_name;
    @NotNull(message = "choose date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @NotNull(message = "choose time")
    private LocalTime time;
    private String status;
    private String home_team_name;
    private String guest_team_name;
    @Min(value = 0, message = "the score cannot be negative")
    private int home_team_score;
    @Min(value = 0, message = "the score cannot be negative")
    private int guest_team_score;
    private LocalDate created;
    private LocalDate updated;

    public Game() {
    }

    public Game(int id, String tournament_name, String place_name, LocalDate date, LocalTime time, String status, String home_team_name, String guest_team_name, int home_team_score, int guest_team_score, LocalDate created, LocalDate updated) {
        this.id = id;
        this.tournament_name = tournament_name;
        this.place_name = place_name;
        this.date = date;
        this.time = time;
        this.status = status;
        this.home_team_name = home_team_name;
        this.guest_team_name = guest_team_name;
        this.home_team_score = home_team_score;
        this.guest_team_score = guest_team_score;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTournament_name() {
        return tournament_name;
    }

    public void setTournament_name(String tournament_name) {
        this.tournament_name = tournament_name;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void statusUpdate(String status) {
        this.status = status;
    }

    public String getHome_team_name() {
        return home_team_name;
    }

    public void setHome_team_name(String home_team_name) {
        this.home_team_name = home_team_name;
    }

    public String getGuest_team_name() {
        return guest_team_name;
    }

    public void setGuest_team_name(String guest_team_name) {
        this.guest_team_name = guest_team_name;
    }

    public int getHome_team_score() {
        return home_team_score;
    }

    public void setHome_team_score(int home_team_score) {
        this.home_team_score = home_team_score;
    }

    public int getGuest_team_score() {
        return guest_team_score;
    }

    public void setGuest_team_score(int guest_team_score) {
        this.guest_team_score = guest_team_score;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }

    public void statusUpdate(Game game){
        if (game.getDate().compareTo(LocalDate.now()) > 0) {
            game.statusUpdate("Expected");
        } else if (game.getDate().compareTo(LocalDate.now()) < 0) {
            game.statusUpdate("Passed");
        } else {
            if (game.getTime().compareTo(LocalTime.now()) == 1) {
                game.statusUpdate("Expected");
            } else {
                game.statusUpdate("Passed");
            }
        }
    }
}
