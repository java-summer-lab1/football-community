package by.kovalevskiy.spring.model;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class User_account {
   private int id;
   @NotEmpty(message = "nickname should not be empty")
   @Size(min = 2,max = 20,message = "allowed from 2 to 20 characters")
   private String nickname;
   private String role;
   private LocalDate created;
   private LocalDate updated;

   public User_account() {
   }

   public User_account(int id, String nickname, String role, LocalDate created, LocalDate updated) {
      this.id = id;
      this.nickname = nickname;
      this.role = role;
      this.created = created;
      this.updated = updated;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getNickname() {
      return nickname;
   }

   public void setNickname(String nickname) {
      this.nickname = nickname;
   }

   public String getRole() {
      return role;
   }

   public void setRole(String role) {
      this.role = role;
   }

   public LocalDate getCreated() {
      return created;
   }

   public void setCreated(LocalDate created) {
      this.created = created;
   }

   public LocalDate getUpdated() {
      return updated;
   }

   public void setUpdated(LocalDate updated) {
      this.updated = updated;
   }
}
