package by.kovalevskiy.spring.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Tournament {
    private int id;
    @NotEmpty(message = "name should not be empty")
    @Size(min = 2,max = 50,message = "allowed from 2 to 50 characters")
    private String name;
    @NotNull(message = "year should not be empty")
    @Min(value = 2023,message = "year must be current or future")
    private int year;
    private LocalDate created;
    private LocalDate updated;
    public Tournament() {
    }
    public Tournament(int id, String name, int year, LocalDate created, LocalDate updated) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }
}
