package by.kovalevskiy.spring.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Comment {
    private int id;
    private String user_account_nickname;
    private int game_id;
    @NotEmpty(message = "you should write something")
    @Size(min = 1,max = 500,message = "allowed from 1 to 500 characters")
    private String text;
    private LocalDate created;
    private LocalDate updated;

    public Comment() {
    }

    public Comment(int id, String user_account_nickname, int game_id, String text, LocalDate created, LocalDate updated) {
        this.id = id;
        this.user_account_nickname = user_account_nickname;
        this.game_id = game_id;
        this.text = text;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_account_nickname() {
        return user_account_nickname;
    }

    public void setUser_account_nickname(String user_account_nickname) {
        this.user_account_nickname = user_account_nickname;
    }

    public int getGame_id() {
        return game_id;
    }

    public void setGame_id(int game_id) {
        this.game_id = game_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }
}
