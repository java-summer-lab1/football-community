package by.kovalevskiy.spring.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Team {
    private int id;
    @NotEmpty(message = "name should not be empty")
    @Size(min = 2,max = 50,message = "allowed from 2 to 50 characters")
    private String name;
    @NotEmpty(message = "city should not be empty")
    @Size(min = 2,max = 50,message = "allowed from 2 to 50 characters")
    private String city;
    private LocalDate created;
    private LocalDate updated;

    public Team() {
    }

    public Team(int id, String name, String city, LocalDate created, LocalDate updated) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.created = created;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }
}
