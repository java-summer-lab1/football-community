package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Tournament;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class TournamentDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public TournamentDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Tournament> showAllTournament(){
        return jdbcTemplate.query("SELECT * FROM tournament", new BeanPropertyRowMapper<>(Tournament.class));
    }
    public Tournament showTournament(int id){
        return jdbcTemplate.query("SELECT * FROM tournament WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(Tournament.class)).stream().findAny().orElse(null);
    }
    public Optional<Tournament> showTournament(String name){
        return jdbcTemplate.query("SELECT * FROM tournament WHERE name=?", new Object[]{name},new BeanPropertyRowMapper<>(Tournament.class)).stream().findAny();
    }

    public void saveTournament(Tournament tournament){
        jdbcTemplate.update("insert into tournament (name, year, created, updated) VALUES (?,?,?,?)",
                tournament.getName(),tournament.getYear(),tournament.getCreated(),tournament.getUpdated());
    }
    public void updateTournament(Tournament tournament) {
        jdbcTemplate.update("update tournament set name=?, year=?, updated=? where id=?",
                tournament.getName(),tournament.getYear(),tournament.getUpdated(),tournament.getId());
    }
    public void deleteTournament(int id) {
        jdbcTemplate.update("delete from tournament where id=?", id);
    }
}
