package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommentDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public CommentDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Comment> showAllGameComments(int id){
        return jdbcTemplate.query("SELECT * FROM comment where game_id=?", new Object[]{id}, new BeanPropertyRowMapper<>(Comment.class));
    }
    public Comment showComment(int id){
        return jdbcTemplate.query("SELECT * FROM comment WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(Comment.class)).stream().findAny().orElse(null);
    }
    public void saveComment(Comment comment){
        jdbcTemplate.update("insert into comment (user_account_nickname, game_id, text, created, updated) VALUES (?,?,?,?,?)",
                comment.getUser_account_nickname(),comment.getGame_id(),comment.getText(),comment.getCreated(),comment.getUpdated());
    }
    public void deleteComment(int id) {
        jdbcTemplate.update("delete from comment where id=?", id);
    }
}
