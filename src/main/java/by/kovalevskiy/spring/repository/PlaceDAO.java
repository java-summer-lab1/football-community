package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Place;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PlaceDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public PlaceDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Place> showAllPlaces(){
        return jdbcTemplate.query("SELECT * FROM place", new BeanPropertyRowMapper<>(Place.class));
    }
    public Place showPlace(int id){
        return jdbcTemplate.query("SELECT * FROM place WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(Place.class)).stream().findAny().orElse(null);
    }
    public Optional<Place> showPlace(String name){
        return jdbcTemplate.query("SELECT * FROM place WHERE name=?", new Object[]{name},new BeanPropertyRowMapper<>(Place.class)).stream().findAny();
    }
    public Optional<Place> showPlaceByAddress(String address){
        return jdbcTemplate.query("SELECT * FROM place WHERE address=?", new Object[]{address},new BeanPropertyRowMapper<>(Place.class)).stream().findAny();
    }
    public void savePlace(Place place){
        jdbcTemplate.update("insert into place (name, city, address, administrator_phone_number, created, updated) VALUES (?,?,?,?,?,?)",
                place.getName(),place.getCity(),place.getAddress(),place.getAdministrator_phone_number(),place.getCreated(),place.getUpdated());
    }
    public void updatePlace(Place place) {
        jdbcTemplate.update("update place set name=?, city=?, address=?, administrator_phone_number=?, updated=? where id=?",
                place.getName(),place.getCity(),place.getAddress(),place.getAdministrator_phone_number(),place.getUpdated(),place.getId());
    }
    public void deletePlace(int id) {
        jdbcTemplate.update("delete from place where id=?", id);
    }
}
