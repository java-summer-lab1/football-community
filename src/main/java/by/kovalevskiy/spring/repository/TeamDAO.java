package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class TeamDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public TeamDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Team> showAllTeams(){
        return jdbcTemplate.query("SELECT * FROM team", new BeanPropertyRowMapper<>(Team.class));
    }
    public Team showTeam(int id){
        return jdbcTemplate.query("SELECT * FROM team WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(Team.class)).stream().findAny().orElse(null);
    }
    public Optional<Team> showTeam(String name){
        return jdbcTemplate.query("SELECT * FROM team WHERE name=?", new Object[]{name},new BeanPropertyRowMapper<>(Team.class)).stream().findAny();
    }
    public void saveTeam(Team team){
        jdbcTemplate.update("insert into team (name, city, created, updated) VALUES (?,?,?,?)",
                team.getName(),team.getCity(),team.getCreated(),team.getUpdated());
    }
    public void updateTeam(Team team) {
        jdbcTemplate.update("update team set name=?, city=?, updated=? where id=?",
                team.getName(),team.getCity(),team.getUpdated(),team.getId());
    }
    public void deleteTeam(int id) {
        jdbcTemplate.update("delete from team where id=?", id);
    }
}
