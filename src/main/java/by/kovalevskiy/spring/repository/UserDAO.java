package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.User_account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<User_account> showAllUser(){
        return jdbcTemplate.query("SELECT * FROM user_account", new BeanPropertyRowMapper<>(User_account.class));
    }
    public User_account showUser(int id){
        return jdbcTemplate.query("SELECT * FROM user_account WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(User_account.class)).stream().findAny().orElse(null);
    }
    public Optional<User_account> showUser(String nickname){
        return jdbcTemplate.query("SELECT * FROM user_account WHERE nickname=?", new Object[]{nickname},new BeanPropertyRowMapper<>(User_account.class)).stream().findAny();
    }
    public void saveUser(User_account user_account){
        jdbcTemplate.update("insert into user_account (nickname, role, created, updated) VALUES (?,?,?,?)",user_account.getNickname(),user_account.getRole(),user_account.getCreated(),user_account.getUpdated());
    }
    public void updateUser(User_account user_account) {
        jdbcTemplate.update("update user_account set nickname=?, updated=? where id=?", user_account.getNickname(),user_account.getUpdated(),user_account.getId());
    }
    public void deleteUser(int id) {
        jdbcTemplate.update("delete from user_account where id=?", id);
    }
}
