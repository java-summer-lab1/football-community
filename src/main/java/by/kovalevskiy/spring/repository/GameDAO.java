package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GameDAO {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public GameDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public List<Game> showAllGames(){
        List<Game> gameList = jdbcTemplate.query("SELECT * FROM game", new BeanPropertyRowMapper<>(Game.class));
        return gameList;
    }
    public Game showGame(int id){
        return jdbcTemplate.query("SELECT * FROM game WHERE id=?", new Object[]{id},new BeanPropertyRowMapper<>(Game.class)).stream().findAny().orElse(null);
    }
    public void saveGame(Game game){
        jdbcTemplate.update("insert into game (tournament_name, place_name, date, time, status, home_team_name, guest_team_name, created, updated) VALUES (?,?,?,?,?,?,?,?,?)",
                game.getTournament_name(),game.getPlace_name(),game.getDate(),game.getTime(),game.getStatus(),game.getHome_team_name(),game.getGuest_team_name(),game.getCreated(),game.getUpdated());
    }
    public void updateGame(Game game) {
        jdbcTemplate.update("update game set tournament_name=?, place_name=?, date=?, time=?, status=?, home_team_name=?, guest_team_name=?, home_team_score=?,guest_team_score=?, updated=? where id=?",
                game.getTournament_name(),game.getPlace_name(),game.getDate(),game.getTime(),game.getStatus(),game.getHome_team_name(),game.getGuest_team_name(),game.getHome_team_score(),game.getGuest_team_score(),game.getUpdated(),game.getId());
    }
    public void updateGameStatus(Game game) {
        jdbcTemplate.update("update game set status=? where id=?", game.getStatus(),game.getId());
    }
    public void deleteGame(int id) {
        jdbcTemplate.update("delete from game where id=?", id);
    }
}
