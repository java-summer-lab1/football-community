package by.kovalevskiy.spring.repository;

import by.kovalevskiy.spring.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class PlayerDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PlayerDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Player> showAllPlayers() {
        return jdbcTemplate.query("SELECT * FROM player", new BeanPropertyRowMapper<>(Player.class));
    }
    public Player showPlayer(int id) {
        return jdbcTemplate.query("SELECT * FROM player WHERE id=?", new Object[]{id}, new BeanPropertyRowMapper<>(Player.class)).stream().findAny().orElse(null);
    }
    public Optional<Player> showPlayer(String name, String surname) {
        return jdbcTemplate.query("SELECT * FROM player WHERE name=? AND surname=?", new Object[]{name, surname}, new BeanPropertyRowMapper<>(Player.class)).stream().findAny();
    }
    public List<Player> showTeamPlayers(int id) {
        List<Integer> integers = jdbcTemplate.queryForList("select player_id from team_2_player where team_id=?", new Object[]{id}, Integer.class);
        List<Player> players= new ArrayList<>();
        for (Integer player_id:integers) {
            players.add(jdbcTemplate.query("SELECT * FROM player WHERE id=?", new Object[]{player_id}, new BeanPropertyRowMapper<>(Player.class)).stream().findAny().orElse(null));
        }
        return players;
    }
    public void savePlayer(Player player) {
        jdbcTemplate.update("insert into player (id, name, surname, age, city, created, updated) VALUES (?,?,?,?,?,?,?)",
                player.getId(), player.getName(), player.getSurname(), player.getAge(), player.getCity(), player.getCreated(), player.getUpdated());
    }
    public void updatePlayer(Player player) {
        jdbcTemplate.update("update player set name=?, surname=?, age=?, city=?, updated=? where id=?",
                player.getName(), player.getSurname(), player.getAge(), player.getCity(), player.getUpdated(), player.getId());
    }
    public void deletePlayer(int id) {
        jdbcTemplate.update("delete from player where id=?", id);
    }
    public void deletePlayerFromTeam(int player_id, int team_id) {
        jdbcTemplate.update("delete from team_2_player where team_id=? and player_id=?",team_id, player_id);
    }
    public void addPlayerToTeam(int team_id,int player_id) {
        jdbcTemplate.update("insert into team_2_player (team_id, player_id) VALUES (?,?)", team_id,player_id);
    }
    public List<Integer> IsTherePlayerInTeam(int team_id,int player_id) {
        List<Integer> integers = jdbcTemplate.queryForList("select player_id from team_2_player where team_id=? and player_id=?", new Object[]{team_id,player_id}, Integer.class);
        return integers;
    }
}
